# _Jade Web Repack_
**_Jade Web Repack_** is a website for Repacks emulators

[![Project Status](https://img.shields.io/badge/Status-Released-blue.svg?style=flat-square)](#)
[![Project Version](https://img.shields.io/badge/version-0.0.1-green.svg?style=flat-square)](#)

| Requirements | Description |
| :----------- | :---------- |
| **PHP Version** | **7.1 or newer** is recommended |
| **Apache Modules** | mod_rewrite - mod_headers - mod_expires - mod_deflate |


## Authors (Developers)

* @perioner - *Back-End Developer and Front-end*

## Links

Web Official [WoWCMS](https://wow-cms.com).

## Copyright

Copyright 2019 [WoWCMS](https://wow-cms.com).
